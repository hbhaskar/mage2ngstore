import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Resp, Product, ProductService } from './services/product.service';
import { SpinnerService } from './services/spinner.service';

@Injectable()
export class ProductsResolver implements Resolve<Observable<Object>> {

  constructor(private dataService: ProductService, private spinner: SpinnerService) {
  }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot  ): Observable<Object> {
        this.spinner.start();
        const sku = route.paramMap.get('sku');
        console.log(state);
        
        return this.dataService.getProducts(sku);
  }
}