import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {Subscriber} from 'rxjs/Subscriber';
import { UserService } from './user.service';
import { CartService } from './cart.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private userService: UserService, private cartService: CartService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let isCartEmpty = true;
    this.cartService.getCartData().subscribe(
      (data) => {
        isCartEmpty = data.items.length;
      })
    if (localStorage.getItem('currentUser') !== null) {
      return true;
    } else {
      this.router.navigate(['/login'], {
        queryParams: {
          return: state.url
        }
      });
      return false;
    }
  }
}
