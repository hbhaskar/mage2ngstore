import { Injectable } from '@angular/core';
//import {HttpClient, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {HttpParams} from "@angular/common/http";
import { DataService } from '../shared/service/data.service';


export interface Product {
  item_id: number;
  name: string;
  price: number;
  sku: string;
  quantity: number;
  qty: number;
  media_gallery_entries: [{file:string}];
      custom_attributes: [{}];
  isAdding: boolean;
    imgSrc: string;
    product_option: {}

}
export interface Resp {
    items: Product[];
    search_criteria: {};
    total_count: number;
    page_size: number;
}

@Injectable()
export class ProductService {

    constructor( private http: HttpClient, private dataService: DataService) { }


    private url =  this.dataService.apiEndPoint + 'rest/V1/products';

    private options = {
                headers: new HttpHeaders({
                    'Content-Type':  'application/json',
                    'Authorization': this.dataService.authorization
                })
            };

    getProducts1(): Observable<Product[]> {
        return this.http.get<Product[]>(this.url, this.options);
    }
    getProduct(id): Observable<Product> {
        return this.http.get<Product>(this.url + '/' + id, this.options);
    }

    getProducts(sku): Observable<Resp>  {

        var search = new URLSearchParams();
        search.set('searchCriteria[filter_groups][0][filters][0][field]', 'visibility');
        search.set('searchCriteria[filter_groups][0][filters][0][value]', '4');
        search.set('searchCriteria[filter_groups][1][filters][0][field]', 'status');
        search.set('searchCriteria[filter_groups][1][filters][0][value]', '1');

        if (sku !== '') {

            search.set('searchCriteria[filter_groups][2][filters][0][field]', 'sku');
            search.set('searchCriteria[filter_groups][2][filters][0][value]', 'W%');
            search.set('searchCriteria[filterGroups][0][filters][0][field]', 'category_id');
            search.set('searchCriteria[filterGroups][0][filters][0][value]', sku );
            search.set('searchCriteria[filter_groups][2][filters][0][condition_type]', 'like');

            // console.log(sku.substring(0,1));
        }
        /*search.set('searchCriteria[filter_groups][1][filters][0][field]', 'type_id');
        search.set('searchCriteria[filter_groups][1][filters][0][value]', 'simple');
        search.set('searchCriteria[filter_groups][1][filters][1][field]', 'type_id');
        search.set('searchCriteria[filter_groups][1][filters][1][value]', 'downloadable');*/


        search.set('searchCriteria[pageSize]', '100');
        search.set('searchCriteria[currentPage]', '1');
        // console.log(search.toString());

        return this.http.get<Resp>(this.url + '?' + search.toString(), this.options);
      }

}
