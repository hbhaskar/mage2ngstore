import {Injectable, EventEmitter} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {Subscriber} from 'rxjs/Subscriber';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { DataService } from '../shared/service/data.service';

@Injectable()
export class UserService {

	constructor(private http: HttpClient, private dataService: DataService) {}

	private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem('userToken')
  });
  private options = { headers: this.headers };

	private loginUrl =  this.dataService.apiEndPoint + 'rest/default/V1/integration/customer/token';
	private authUrl = this.dataService.apiEndPoint + 'rest/V1/customers/me';
	private userName: string;
	private password: string;
	private userToken: string;
	private userData: {};

	login(username: string, password: string){
		this.userName = username;
		this.password = password;
		this.authenticate();
	}

	authenticate(){
		this.http.post<any>(this.loginUrl, { username: this.userName, password: this.password }).subscribe(
			(token) => {
				this.userToken = token;
				localStorage.setItem('userToken', token);
				let myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.userToken });
				let options = { headers: myHeaders };
				if (this.userToken){
					this.http.get<any>(this.authUrl, options).subscribe(
						data => {
							this.userData = data;
							localStorage.setItem('currentUser', JSON.stringify(this.userData));
						}
					)
				}
			})
		}

		getUserData(): Observable<any>{
			return this.http.get(this.authUrl, this.options);
		}

		getShippingMethods(): Observable<any>{
			return this.http.get<any>(this.dataService.apiEndPoint + 'rest/V1/carts/mine/shipping-methods', this.options);
    }

		isLoggedIn() {
			if (localStorage.getItem('currentUser') === null) {
				return false;
			}
			return true;
		}

		reset() {
			// remove user from local storage to log user out
			localStorage.removeItem("cart-id");
			localStorage.removeItem("userToken");
			localStorage.removeItem("currentUser");
		}

		logout() {
			this.reset();
		}
	}
