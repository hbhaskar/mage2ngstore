import {Injectable, EventEmitter} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { Product } from './product.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import {Subscriber} from 'rxjs/Subscriber';
import { DataService } from '../shared/service/data.service';
import { UserService } from './user.service';


export interface Item {
  item_id: number;
  price: number;
  base_price: number;
  qty: number;
  row_total: number;
  base_row_total: number;
  row_total_with_discount: number;
  tax_amount: number;
  base_tax_amount: number;
  tax_percent: number;
  discount_amount: number;
  base_discount_amount: number;
  discount_percent: number;
  price_incl_tax: number;
  base_price_incl_tax: number;
  row_total_incl_tax: number;
  base_row_total_incl_tax: number;
  options: string;
  weee_tax_applied_amount: number;
  weee_tax_applied: string;
  extension_attributes: {};
  name: string;
}

export interface Totals {
  grand_total: number;
  base_grand_total: number;
  subtotal: number;
  base_subtotal: number;
  discount_amount: number;
  shipping_amount: number;
  base_currency_code: string;
  items_qty: number;
  items: Item[];
}

@Injectable()
export class CartService {

  private _STORAGE_KEY = 'cart-id';
  private token = this.userService.isLoggedIn() ? 'Bearer ' + localStorage.getItem('userToken') : this.dataService.authorization;

  private headers = new Headers({
    'Content-Type': 'application/json',
    'Authorization': this.token
  });
  private options = new RequestOptions({headers: this.headers});

  refreshEvent: EventEmitter<any> = new EventEmitter();
  isLoading = false;
  isConfirmed = false;
  totals: Totals;

  constructor(private _http: Http, private dataService: DataService, private userService: UserService) {
  }

  reset() {
    localStorage.removeItem(this._STORAGE_KEY);
  }

  add(product: Product) {
    return new Observable<Product[]>((observer: Subscriber<any>) => {
      this.getCardId().subscribe(cartId => {
        const cartApi = this.userService.isLoggedIn() ? 'rest/V1/carts/mine' : 'rest/default/V1/guest-carts/' + cartId;
        let cartItems: any[] = [];
        const body = JSON.stringify({
          cartItem: {
            quote_id: cartId,
            sku     : product.sku,
            qty     : this.getProductQuantity(product),
            product_option: {
              extension_attributes: {
                configurable_item_options: [
                  {
                    option_id: 93,
                    option_value: 49
                  },
                  {
                    option_id: 142,
                    option_value: 168
                  }
                ]
              }
            }
          }
        });

        return this._http.post(this.dataService.apiEndPoint + cartApi + '/items', body, this.options)
        .map(response => response.json())
        .subscribe(cartItem => {

              let cartProd = cartItem;
              cartProd["imgSrc"] = (product.custom_attributes.filter((el) => el["attribute_code"] === 'thumbnail').map((obj) => {return obj["value"]}).join());
              cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
              
              const ifExists = cartItems.find((obj) => {return obj.sku === cartProd.sku});
              
              if(!ifExists){
                  cartItems.push(cartProd);
              }
              localStorage.setItem('cartItems', JSON.stringify(cartItems));

          observer.next(cartItem);
        });
      });

    });
  }

  getProductQuantity(product) {
    if (product.quantity) {
      return product.quantity;
    } else {
      return '1';
    }
  }

  del(item: Item) {

    this.isLoading = true;

    this.getCardId().subscribe(cartId => {
      const cartApi = this.userService.isLoggedIn() ? 'rest/V1/carts/mine' : 'rest/V1/guest-carts/' + cartId;
      const body = JSON.stringify({
        cartItem: {
          quote_id: cartId,
          item_id     : item.item_id,
          qty     : 0
        }
      });


      return this._http.delete(this.dataService.apiEndPoint + cartApi + '/items/' + item.item_id, this.options)
      .map(response => response.json())
      .subscribe(isSucceeded => {

        if (isSucceeded) {
          this.refresh();
          let cartItems = JSON.parse(localStorage.getItem('cartItems'));
          if(cartItems.length){
            const ifExists = cartItems.find((obj) => {return obj.item_id === item.item_id});
          }

        } else {
          // Todo: handle error here
          this.isLoading = false;
        }

      });
    });

  }

  clearFullCart(itemClear) { // deleting from the cart based upon the checkbox selected
    return new Observable<any>((observer: Subscriber<any>) => {
    this.getCardId().subscribe(cartId => {
      const cartApi = this.userService.isLoggedIn() ? 'rest/V1/carts/mine' : 'rest/V1/guest-carts/' + cartId;
      for (let i = 0; i < itemClear.length; i++) { // iterate through the item id
        const itemClearLoop = this._http.delete(this.dataService.apiEndPoint + cartApi + '/items/' + itemClear[i].item_id, this.options)
        .map(response => response.json())
        .catch(this._handleError)
        .subscribe(data => {

          observer.next(data);

        });
      }
    },
  );
});
}

changeQty(event, itemId){
  return new Observable<Product[]>((observer: Subscriber<any>) => {

    this.getCardId().subscribe(cartId => {
      const cartApi = this.userService.isLoggedIn() ? 'rest/V1/carts/mine' : 'rest/V1/guest-carts/' + cartId;
      const body = JSON.stringify({
        cartItem: {
          item_id: itemId,
          qty: event,
          quote_id: cartId
        }
      });

      return this._http.put(this.dataService.apiEndPoint + cartApi + '/items/' + itemId, body, this.options)
      .subscribe(cartItem => {
        this.refresh();

      });
    });

  });
}

refresh() {

  this.isLoading = true;

  this.getCardId().subscribe(cartId => {
    const cartApi = this.userService.isLoggedIn() ? 'rest/V1/carts/mine' : 'rest/V1/guest-carts/' + cartId;
    return this._http.get(this.dataService.apiEndPoint + cartApi + '/totals', this.options)
    .map(res => <Totals>res.json())
    .catch(this._handleError)
    .subscribe(totals => {
      this.totals = totals;

      this.refreshEvent.emit(this.totals);

      this.isLoading = false;
    });
  });

}

getCardId(): Observable<string> {
  if(!this.userService.isLoggedIn()){
    var cartId = localStorage.getItem(this._STORAGE_KEY);

    if (cartId) {

      return Observable.of(cartId);

    } else {


      return this._http.post(this.dataService.apiEndPoint + 'rest/V1/guest-carts', '', this.options)
      .map(response => {

        var cartId = response.json();
        localStorage.setItem(this._STORAGE_KEY, cartId);

        return cartId;

      })
      .catch(this._handleError);

    }
  }else{
    return this._http.post(this.dataService.apiEndPoint + 'rest/V1/carts/mine', '', this.options)
    .map(response => {

      var cartId = response.json();
      localStorage.setItem(this._STORAGE_KEY, cartId);

      return cartId;

    })
    .catch(this._handleError);
  }
}

getCartData() {
  return new Observable<any>((observer: Subscriber<any>) => {

    this.getCardId().subscribe(cartId => {
      const cartApi = this.userService.isLoggedIn() ? 'rest/V1/carts/mine' : 'rest/V1/guest-carts/' + cartId;

      return this._http.get(this.dataService.apiEndPoint + cartApi, this.options)
      .map(response => <any>response.json())
      .catch(this._handleError)
      .subscribe(data => {

        observer.next(data);

      });
    });
  });
}

getCartImage(sku): Observable<string> { // cart image
  let cartItems = JSON.parse(localStorage.getItem('cartItems'));
  
  let imgSrc = cartItems.find((item) => {
    return item.sku === sku;
  });
  return Observable.of(imgSrc);
}

private _handleError(error: Response) {
  // in a real world app, we may send the error to some remote logging infrastructure
  // instead of just logging it to the console
  console.error(error.json());
  return Observable.throw(error.json() || 'Server error');
}
}
