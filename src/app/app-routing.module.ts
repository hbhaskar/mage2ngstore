import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CartComponent } from './cart/cart.component';
import { ProductComponent } from './product/product.component';
import { ProductlistComponent } from './productlist/productlist.component';
import { ProductsResolver } from './products.resolver';
import { ProductResolver } from './product/product.resolver';
import { AuthComponent } from './auth/auth.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { AuthGuardService } from './services/authguard.service';
import { ShippingDataResolver } from './checkout/shipping.resolver';

const routes: Routes = [
        { path: '', redirectTo: '/home', pathMatch: 'full' },
        { path: 'home', component: HomeComponent },
        { path: 'customer', component: HomeComponent },
        { path: 'cart', component: CartComponent },
        { path: 'login', component: AuthComponent },
        { path: 'productlist', component: ProductlistComponent},
        { path: 'productlist/:sku', component: ProductlistComponent, resolve: {
            products: ProductsResolver
        } },
        { path: 'pdp/:id', component: ProductComponent , resolve: {
            product: ProductResolver
        }},
        {path: 'checkout', component: CheckoutComponent, canActivate: [AuthGuardService]},
        {path: 'order', component: CheckoutComponent, resolve: {
            amount: ShippingDataResolver
        } }
    ];
@NgModule({
  imports: [ RouterModule.forRoot(routes, { enableTracing: false }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
