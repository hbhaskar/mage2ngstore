import { Component, OnInit, Input, SimpleChanges, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { ProductService, Product } from '../services/product.service';
import { SpinnerService } from '../services/spinner.service';
import { DataService } from '../shared/service/data.service';
import { CartService } from '../services/cart.service';
declare var $:any;

export interface Color {
  color: string;
  checked: boolean;
}

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit {

  products: Product[];
  @Input() loading: boolean;
  message: string;
  counter: number;
  itemPrice = 0;
  test: any;
  colors: any;
  sizes = [];


  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private productService: ProductService, public spinner: SpinnerService,  private cartService: CartService,  private dataService: DataService) {
    console.log('Products - Contructor');

  }

    ngOnActivate() {
       console.log('Products onActivate');
    }
    ngOnInit() {
        this.route.data.subscribe( (resp) => {
            console.log(resp);

            this.products = resp.products.items.map((i) => {return {
                name: i.name, 
                color: (i.custom_attributes.filter((el) => el["attribute_code"] === 'color').map((obj) => {return obj.value}).join()),
                id: i["id"],
                imgSrc: i["imgSrc"],
                price: i["price"],
                sku: i["sku"],
                size: i.custom_attributes.filter((el) => el["attribute_code"] === 'size').map((obj) => {return obj.value}),
                custom_attributes: i.custom_attributes
            }});
            
            this.products.forEach((product: any) => {
                    product.custom_attributes.forEach((attr: any) => {
                    if (attr.attribute_code === 'small_image') {
                        product.imgSrc = this.dataService.apiEndPoint +'pub/media/catalog/product' + attr.value;
                    }
                });
            });

            this.colors = this.products.map(
                    (item) => item["color"]).filter((value, index, self) => {if(value !== '') return self.indexOf(value) === index})
            .map(function(el){ let obj = {}; obj["color"] = el; obj["checked"] = false; return obj});
            
            this.spinner.stop();
        }
    );

    this.dataService.currentMessage.subscribe(message => this.message = message); // Search Filter
  }

  ngAfterViewInit(){
      $('.ui.checkbox')
            .checkbox()
        ;
  }

  addToCart(item) {
    this.spinner.start();
      this.cartService.add(item).subscribe(data => {
        this.cartService.getCartData().subscribe(cartNumber => { // Add to cart increment
            this.counter = cartNumber.items_qty;
            this.dataService.incrementCounter(this.counter);
            this.spinner.stop();
        });
      });
  }
    ngOnChanges (changes: SimpleChanges) {
        console.log('Products on change');
    }

    getProducts(): void {
        const sku = this.route.snapshot.paramMap.get('sku');
    }

    selectedColors() {
        return this.colors.reduce((colors, color) => {
        if (color.checked) {
            colors.push(color.color);
        }
        return colors;
        }, [])
    }

    ngOnDestroy() {
        // this.spinner.start();
       
    }


}
