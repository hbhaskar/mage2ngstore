import { Component, OnInit, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Subscriber} from 'rxjs/Subscriber';
import { UserService } from '../services/user.service';
import { DataService } from '../shared/service/data.service';
import { CartService } from '../services/cart.service';
declare var $:any;

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})

export class CheckoutComponent implements OnInit {

constructor(private http: HttpClient, private userService: UserService, private dataService: DataService,
  private cartService: CartService, private router: Router, private route: ActivatedRoute) { }

  private userData: {};
  private customerCart: {};
  shippingData: any;
  products: any[];
  total: number;
  hideContent: boolean;
  @ViewChild('accordion') accordion: ElementRef;

  ngOnInit() {
    this.getUserData();
    this.hideContent = this.router.url === '/order' ? true : false;
   
  }

  ngAfterViewChecked() {

  }

  getUserData() {
    this.userService.getUserData()
    .map(res => res.addresses)
    .subscribe(
      (data) => {
        this.userData = data[0];
        if (this.userData.hasOwnProperty('country_id')) {
          this.getCountry(this.userData["country_id"]).subscribe(
            (resp) => {this.userData['country'] = resp.name});
        }
        this.getUserCart();
    })
  }

  getUserCart() {
    this.cartService.getCartData().subscribe(
      (data) => {
        this.customerCart = data;
        this.products = data.items;
        this.total = this.products.reduce(
          (sum, o) => {
            return sum + o.price;
          }, 0);
        this.shippingData = this.route.snapshot.data;
         if(data.items.length > 0) {
          $('.ui.accordion')
            .accordion();
        }
      });
    }

    getCountry(id): Observable<any> {
      return this.http.get('https://restcountries.eu/rest/v2/alpha/' + id);
    }
  }
