import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ShippingDataResolver implements Resolve<Observable<number>> {

  constructor() {
  }

  public resolve() {
        let amount = 15;
        return Observable.of(amount);
  }
}