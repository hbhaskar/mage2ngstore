import { Component, OnInit, Input } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Subscriber} from 'rxjs/Subscriber';
import { Resp, Product, ProductService } from '../services/product.service';
import { CartService } from '../services/cart.service';
import { DataService } from '../shared/service/data.service';
import { SpinnerService } from '../services/spinner.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

	@Input() products: Product[];
  itemsChoosed: any[] = [];
  itemsEmpty: boolean;
  counter: number;
  chooseCart: boolean;
  selectedAll: any;
  deSelect: boolean;
  noProducts: boolean;

  constructor(private cartService: CartService, private http: Http, private dataService: DataService, private spinner: SpinnerService) {

  }

  private imgUrl = this.dataService.apiEndPoint + 'rest/V1/products/';
  private headers = new Headers({
          'Content-Type': 'application/json',
          'Authorization': this.dataService.authorization
        });
  private options = new RequestOptions({headers: this.headers});
  private imgPathPrefix = "http://localhost/magento2ng5/pub/media/catalog/product";

  ngOnInit() {
  	this.getCartItems();
  }

  getCartItems(){
    this.spinner.start();
  	this.cartService.getCartData().subscribe(data => {
      this.counter = 0;
      this.products = data.items;
       // added to fetch the product image
       if(this.products.length){
          this.products.map((prod) => {
            this.cartService.getCartImage(prod.sku).subscribe(path => {
              prod.imgSrc = this.imgPathPrefix + path["imgSrc"];
          })
        })
       }
        
     for (let i = 0; i < this.products.length; i++) {
        this.counter = this.counter + this.products[i].qty;
     }
      this.dataService.incrementCounter(this.counter);
      this.spinner.stop();
    });
  }

  updateQty(event, itemId){
    this.spinner.start();
    this.cartService.changeQty(event, itemId).subscribe(data => {
    });
    this.cartService.refreshEvent.subscribe(cartNumber => {
      const counter = cartNumber.items_qty;
      this.dataService.incrementCounter(counter);
      this.getCartItems();
    });
  }

  delete(item){
    this.cartService.del(item);
    this.products.splice(this.products.indexOf(item),1);
    this.cartService.refreshEvent.subscribe(cartNumber => {
        let counter = cartNumber.items_qty;
        this.dataService.incrementCounter(counter);
    });
  }

  checkAll(event) { // select all checkbox based upon that pushing the values to new array
    this.itemsChoosed = [];
    if (event.target.checked === true) {
      this.itemsEmpty = false;
      if (this.products.length) {
        for (let i = 0; i < this.products.length; i++) {
          this.itemsChoosed.push(this.products[i]);
        }
      }
    } else {
      for (let i = 0; i < this.products.length; i++) {
        this.itemsChoosed.splice(this.itemsChoosed.indexOf(this.products[i]), 1);
      }
    }

  }
  checkValue(isSelected, items) {  // check if the checkbox are selected based upon that pushing the values to new array
    if (isSelected === true) {
      this.itemsChoosed.push(items);
      this.itemsEmpty = false;
    } else {
      this.itemsChoosed.splice(this.itemsChoosed.indexOf(items), 1);
    }

  }
  clearCart(deSelect): void { // removing the items from the cart
    if (this.itemsChoosed.length > 0) {
      this.spinner.start();
      this.cartService.clearFullCart(this.itemsChoosed).subscribe(cartNumber => {
        this.getCartItems();
        this.deSelect = false;
        this.itemsEmpty = false;
      });
    } else {
      this.itemsEmpty = true;
    }
  }

}
