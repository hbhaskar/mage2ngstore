import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CartService } from '../../services/cart.service';
import { DataService } from '../service/data.service';

@Component({
  selector: 'thumbnail',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})

export class ThumbComponent implements OnInit {
  fullImagePath: string;
  private imgUrl = this.dataService.apiEndPoint + 'rest/V1/products/';

  constructor(private cartService: CartService, private http: HttpClient, private dataService: DataService) {
    
  }

  ngOnInit() {
  	
  }

  

}