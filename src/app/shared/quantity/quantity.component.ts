import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'quantity',
  templateUrl: './quantity.component.html',
  styleUrls: ['./quantity.component.css']
})

export class QuantityComponent{
	constructor(){}

	@Input()
	quantity:number;
	
	addItem(): void{
		this.quantity++;
		this.itemCount.emit(this.quantity);
	}

	removeItem(): void{
		if(this.quantity > 0){
			this.quantity--;
			this.itemCount.emit(this.quantity);
		}
	}

	@Output() itemCount = new EventEmitter<any>()
    
}