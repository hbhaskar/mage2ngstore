import { NgModule } from '@angular/core';
import { QuantityComponent } from './quantity/quantity.component';
import { SearchFilterPipe } from './pipes/search-filter.pipe';
import { ProductColorPipe } from './pipes/color-filter.pipe';
import { DataService } from './service/data.service';

@NgModule({
   declarations: [QuantityComponent, SearchFilterPipe, ProductColorPipe],
   exports: [QuantityComponent, SearchFilterPipe, ProductColorPipe],
   providers: [DataService] // Share data among components
})
export class SharedModule { }
