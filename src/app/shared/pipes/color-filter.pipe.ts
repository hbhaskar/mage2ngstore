import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'color'
})


export class ProductColorPipe implements PipeTransform {
  public transform(items: any[], colors: string[]): any[] {
    if (!colors || colors.length === 0) return items;
    return items.filter(
        item => colors.includes(item.color));
  }
}

