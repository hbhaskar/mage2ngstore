import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationStart } from "@angular/router";
import { Observable } from 'rxjs/Observable';
import { DataService } from '../app/shared/service/data.service';
import { CartService } from '../app/services/cart.service';
import { UserService } from '../app/services/user.service';
import { SpinnerService } from '../app/services/spinner.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  name = 'Arctic Fox';
  counter: any;
  routeHidden = true;
  products: any;
  cartHidden = false;

  constructor(private dataService: DataService, private cartService: CartService, private userService: UserService, private spinner: SpinnerService, private router: Router) {}

  searchProducts(searchInput) { // Search Products 
    this.dataService.updateSearch(searchInput);
  }

  ngOnInit() {

    this.cartService.getCartData().subscribe(data => { 
      this.products = data.items;
      if (data.items.length === 0) {
        this.counter = 0;
      } else {
        for (let i = 0; i < this.products.length; i++) {
          this.counter = this.counter + this.products[i].qty;
       }
      }
    });

    // Add to cart increment from product and product list
    this.spinner.start();
    this.dataService.counterMessage.subscribe(message => this.counter = message);
    this.counter = 0;
    this.spinner.stop();

    this.router.events.subscribe( (e) => {
      if (e instanceof NavigationStart) {
        if (e.url === "/login") {
            this.routeHidden = false;
        } else {
            this.routeHidden = true;
        }
      }
    })
  }

  isLoggedIn(){
	    return this.userService.isLoggedIn();
  };
    
  isCartHidden(){
	    this.router.events.subscribe( (e) => {
      if (e instanceof NavigationStart) {
        if (e.url === "/checkout" || e.url === "/order") {
            this.cartHidden = true;
        } else {
            this.cartHidden = false;
        }
      }
    })
    return this.cartHidden;
	};

	logout(){
		this.userService.logout();
	}
}
