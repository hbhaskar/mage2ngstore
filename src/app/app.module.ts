import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';

import { SharedModule } from './shared/shared.module';
import { FooterComponent } from './shared/components/footer/footer.component';

import { AppComponent } from './app.component';
import { FeaturedComponent } from './featured/featured.component';
import { CartComponent } from './cart/cart.component';
import { CatalogComponent } from './catalog/catalog.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CountryComponent } from './country/country.component';
import { MeComponent } from './me/me.component';
import { NavComponent } from './nav/nav.component';
import { BreadcrumbComponent } from './nav/breadcrumb.component';
import { PaymentComponent } from './payment/payment.component';
import { ShippingComponent } from './shipping/shipping.component';
import { ProductComponent } from './product/product.component';
import { ProductlistComponent } from './productlist/productlist.component';
import { AuthComponent } from './auth/auth.component';

import { ProductService } from './services/product.service';
import { UserService } from './services/user.service';
import { SpinnerService } from './services/spinner.service';
import { CartService } from './services/cart.service';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { ProductsResolver } from './products.resolver';
import { ProductResolver } from './product/product.resolver';
import { SpinnerComponent } from './spinner/spinner.component';
import { AuthGuardService } from './services/authguard.service';
import { ShippingDataResolver } from './checkout/shipping.resolver';

import { SafeHtmlPipe } from './pipes/pipe.safehtml';

import {NgxPaginationModule} from 'ngx-pagination';


@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    FeaturedComponent,
    CartComponent,
    CatalogComponent,
    CheckoutComponent,
    CountryComponent,
    MeComponent,
    NavComponent,
    BreadcrumbComponent,
    PaymentComponent,
    ShippingComponent,
    ProductComponent,
    ProductlistComponent,
    HomeComponent,
    SpinnerComponent,
    SafeHtmlPipe,
    FooterComponent
  ],
  imports: [
    BrowserModule,
      HttpClientModule,
      HttpModule,
      AppRoutingModule,
      SharedModule,
      FormsModule, // Imported to use ngModel
      NgxPaginationModule, // Pagination
      NgProgressModule.forRoot({min: 20, meteor: true}),
      NgProgressHttpModule
  ],
  providers: [ProductService, ProductsResolver, ProductResolver, SpinnerService, CartService, UserService, AuthGuardService, ShippingDataResolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
