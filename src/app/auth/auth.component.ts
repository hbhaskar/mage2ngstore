import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { DataService } from '../shared/service/data.service';

@Component({
  templateUrl: './auth.component.html'
})

export class AuthComponent {
	model: any = {};
	return: string = '';
	private userData: {};

  constructor(private userService: UserService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.return = this.route.snapshot.queryParams['return'] || '/';
  }

  private login(){
	    this.userService.login(this.model.username, this.model.password);
	    this.router.navigateByUrl(this.return);
    }
}
