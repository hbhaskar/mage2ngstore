import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Product, ProductService } from '../services/product.service';
import { SpinnerService } from '../services/spinner.service';

@Injectable()
export class ProductResolver implements Resolve<Observable<Product>> {

  constructor(private dataService: ProductService, 
               private spinner: SpinnerService) {
  }

  public resolve( route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot ): Observable<Product> {
        
        this.spinner.start();
        const sku = route.paramMap.get('id');
        console.log('Resolver ' + sku);
        
        return this.dataService.getProduct(sku);
  }
}