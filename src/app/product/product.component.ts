import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService, Product } from '../services/product.service';
import { CartService } from '../services/cart.service';
import { SpinnerService } from '../services/spinner.service';
import { DataService } from '../shared/service/data.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  @Input() item: Product;
  cartItems = [];
  @ViewChild('preview') preview: ElementRef;
  counter: number;

  constructor( private route: ActivatedRoute,
    private productService: ProductService,
    private cartService: CartService,
    public spinner: SpinnerService, private dataService: DataService) { }

    ngOnInit() {
      this.getProduct();
    }
    getProduct(): void {
      const id = this.route.snapshot.paramMap.get('id');

      this.route.data
      .subscribe(product => {
        this.item = product.product;
        this.spinner.stop();
      });
    }

    addItem(items) {
      this.item.quantity = items;
    }

    addToCart(item) {
      this.cartService.add(item).subscribe(data =>{
        this.cartService.getCartData().subscribe(cartNumber => {
          this.counter = cartNumber.items_qty;
          this.dataService.incrementCounter(this.counter);
          this.spinner.stop();
        });
      });
    }

    showThumb(event) {
      this.preview.nativeElement.src = event.target.src;
    }
  }
